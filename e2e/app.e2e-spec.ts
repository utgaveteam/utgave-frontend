import { UtgavePage } from './app.po';

describe('utgave App', function() {
  let page: UtgavePage;

  beforeEach(() => {
    page = new UtgavePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
