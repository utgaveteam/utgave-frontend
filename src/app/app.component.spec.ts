/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Router} from '@angular/router';
import {ComponentFixture} from '@angular/core/testing';
import {Location} from '@angular/common'

import {AppComponent} from './app.component';
import {RouterStub} from "../testing/router-stubs";
import {By} from "@angular/platform-browser";
import {LocationStub} from "../testing/location-stub";

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    //noinspection SpellCheckingInspection
    const projectKey = 'adsjdsja4342dsad';

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [AppComponent],
            providers: [
                {provide: Router, useClass: RouterStub},
                {provide: Location, useClass: LocationStub}
            ],
            schemas: [NO_ERRORS_SCHEMA]
        });
        TestBed.compileComponents();
    });

    beforeEach(inject([Location], (location: LocationStub) => {
        //noinspection SpellCheckingInspection
        const url = `/projects/${projectKey}/dashboard`;
        location.setPath(url);
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

    }));

    it('should create the app', async(() => {
        expect(component).toBeTruthy();
    }));

    it('should navigate to new projects form', async(inject([Router], (router: Router) => {
        let routerSpy = spyOn(router, 'navigateByUrl');
        component.onProjectSelected(null);
        expect(routerSpy.calls.first().args[0]).toBe('/projects/new');
    })));

    it('should navigate to project dashboard component', async(inject([Router], (router: Router) => {
        let routerSpy = spyOn(router, 'navigateByUrl');

        //noinspection SpellCheckingInspection
        const projectKey = 'dsadjsa24819324d';
        const project = {id: 'TPO', name: 'Test project', key: projectKey, creation_date: null};

        component.onProjectSelected(project);
        expect(routerSpy.calls.first().args[0]).toBe(`/projects/${projectKey}/dashboard`);
    })));

    it('should set current projectKey on init if key in url', () => {
        expect(component.projectKey).toBe(projectKey);
    });

    it('should open and close navigation', async(() => {
        expect(component.navigationOpen).toBeFalsy();
        let navigation = fixture.debugElement.query(By.css('div.navigation.closed'));
        expect(navigation).toBeTruthy();

        component.onToggleNavigationClick();
        fixture.detectChanges();
        expect(component.navigationOpen).toBeTruthy();
        navigation = fixture.debugElement.query(By.css('div.navigation.open'));
        expect(navigation).toBeTruthy();
    }));
});
