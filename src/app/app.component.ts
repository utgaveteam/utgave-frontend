import {Component, AfterViewInit, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router'
import {Location} from '@angular/common'

import {Project} from "./models/project";
import {ViewChild} from '@angular/core';
import {ProjectSelectorComponent} from "./project-selector/project-selector.component";

declare var $: (s?: string) => jQuery;

@Component({
    selector: 'utgave-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit {
    navigationOpen: boolean = false;
    projectKey: string;
    @ViewChild('projectSelector') private projectSelector: ProjectSelectorComponent;

    constructor(private router: Router, private ngZone: NgZone, private location: Location) {
    }

    ngAfterViewInit(): void {
        this.ngZone.runOutsideAngular(() => $('[data-toggle="tooltip"]').tooltip());
    }

    ngOnInit() {
        const matches = this.location.path(false).match(/\/projects\/([^/]+)/);
        this.projectKey = matches == null ? null : matches[1];
    }

    onProjectSelected(project: Project) {

        if (null == project) {
            this.router.navigateByUrl('/projects/new');
        }
        else {
            this.projectKey = project.key;
            this.router.navigateByUrl(`/projects/${project.key}/dashboard`);
        }
    }

    onToggleNavigationClick() {
        this.navigationOpen = !this.navigationOpen;
    }
}
