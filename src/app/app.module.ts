import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {ProjectDashboardComponent} from './project-dashboard/project-dashboard.component';
import {ProjectSelectorComponent} from './project-selector/project-selector.component';
import {ProjectService} from './services/projects.service';
import {ProjectFormComponent} from './project-form/project-form.component';
import {ReleaseService} from "./services/release.service";
import {WelcomeComponent} from './welcome/welcome.component';
import {BacklogComponent} from './backlog/backlog.component';
import {QuickIssueComponent} from './quick-issue/quick-issue.component';
import {IssueComponent} from './issue/issue.component';
import {IssueService} from "./services/issue.service";
import {QuickTaskComponent} from './quick-task/quick-task.component';
import {TaskComponent} from './task/task.component';
import {ReleaseListComponent} from './release-list/release-list.component';
import {QuickReleaseComponent} from './quick-release/quick-release.component';
import {ReleaseStatusPipe} from './pipes/release-status.pipe';
import {ReleaseComponent} from './release/release.component';
import {UiModule} from "./ui/ui.module";
import {BoardComponent} from './board/board.component';

const appRoutes: Routes = [
    {path: '', component: WelcomeComponent},
    {path: 'projects/new', component: ProjectFormComponent},
    {path: 'projects/:project_key/dashboard', component: ProjectDashboardComponent},
    {path: 'projects/:project_key/backlog', component: BacklogComponent},
    {path: 'projects/:project_key/releases', component: ReleaseListComponent},
    {path: 'projects/:project_key/releases/:release_key', component: ReleaseComponent},
    {path: 'projects/:project_key/releases/:release_key/board', component: BoardComponent},
    // {path: 'projects/:project_key/issues/:issue_key/task/new', component: },
    // {path: 'projects/:project_key/issues/:issue_key/task/:task_key', component: },
    // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        ProjectDashboardComponent,
        ProjectSelectorComponent,
        ProjectFormComponent,
        WelcomeComponent,
        BacklogComponent,
        QuickIssueComponent,
        IssueComponent,
        QuickTaskComponent,
        TaskComponent,
        ReleaseListComponent,
        QuickReleaseComponent,
        ReleaseStatusPipe,
        ReleaseComponent,
        BoardComponent,
    ],
    imports: [
        UiModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [
        ProjectService,
        ReleaseService,
        IssueService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
