// /* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {BacklogComponent} from './backlog.component';
import {Component} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Issue} from "../models/issue";
import {ProjectService} from "../services/projects.service";
import {FormsModule} from '@angular/forms';
import {ActivatedRouteStub} from "../../testing/router-stubs";
import {ActivatedRoute, RouterModule} from '@angular/router';

class ProjectServiceStub {
    public getBacklog(projectKey): Observable<Issue[]> {
        return Observable.of(null);
    }

    public saveIssue(projectKey, title, kind): Observable<Issue> {
        return Observable.of(null);
    }
}

@Component({
    selector: 'app-quick-issue',
    template: ''
})
class QuickIssueComponentStub {

}

describe('BacklogComponent', () => {
    let component: BacklogComponent;
    let fixture: ComponentFixture<BacklogComponent>;
    //noinspection SpellCheckingInspection
    const projectKey = 'dlksjadlas434535';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, RouterModule],
            declarations: [
                BacklogComponent,
                QuickIssueComponentStub
            ],
            providers: [
                {provide: ProjectService, useClass: ProjectServiceStub},
                {provide: ActivatedRoute, useClass: ActivatedRouteStub}
            ]
        })
            .compileComponents();
    }));

    beforeEach(inject([ActivatedRoute], (route: ActivatedRouteStub) => {
        route.testParams = {project_key: projectKey};
        fixture = TestBed.createComponent(BacklogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set creating issue flag', () => {
        component.onCreateIssue();
        expect(component.creatingIssue).toBeTruthy();
    });

    it('should unset creating issue flag, save issue and update backlog', inject([ProjectService], (service: ProjectService) => {
        //noinspection SpellCheckingInspection
        const issueKey = 'dsadkljasld';
        const issues = [{
            id: 'TPO.1',
            title: 'Test Bug',
            desc: '',
            status: 100,
            key: issueKey,
            creation_date: new Date,
            kind: 'B'
        }];
        let spySave = spyOn(service, 'saveIssue').and.returnValue(Observable.of(issues));
        let spyGetBacklog = spyOn(service, 'getBacklog').and.returnValue(Observable.of(issues));
        component.onCreateIssue();
        expect(component.creatingIssue).toBeTruthy();
        component.onIssueCreated({title: 'Test Issue', kind: 'F'});
        expect(component.creatingIssue).toBeFalsy();
        expect(spySave.calls.count()).toBe(1);
        expect(spySave.calls.first().args[0]).toBe(projectKey);
        expect(spySave.calls.first().args[1]).toBe('Test Issue');
        expect(spySave.calls.first().args[2]).toBe('F');

        expect(spyGetBacklog.calls.count()).toBe(1);
        expect(spyGetBacklog.calls.first().args[0]).toBe(projectKey);
    }));
});
