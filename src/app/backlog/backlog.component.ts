import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {ProjectService} from "../services/projects.service";
import {Issue} from "../models/issue";
import {ActivatedRoute, Params} from '@angular/router';
import {IssueComponent} from "../issue/issue.component";
import {ReleaseService} from "../services/release.service";

@Component({
    selector: 'app-backlog',
    templateUrl: './backlog.component.html',
    styleUrls: ['./backlog.component.scss']
})
export class BacklogComponent implements OnInit {
    issues: Issue[];
    creatingIssue: boolean = false;
    projectKey: string;
    dragging: boolean = false;
    @Input() showHeader: boolean = true;
    @ViewChild('issueDialog') issueDialog: IssueComponent;
    @Output('backlogchanged') backlogChanged = new EventEmitter();

    constructor(private route: ActivatedRoute, private projectService: ProjectService, private releaseService: ReleaseService) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.projectKey = params['project_key'];
            this.reloadIssues();
        });
    }

    onCreateIssue() {
        this.creatingIssue = true;
    }

    onIssueCreated($event) {
        this.creatingIssue = false;
        this.projectService.saveIssue(this.projectKey, $event.title, $event.kind).subscribe(issue => {
            this.reloadIssues();
        });
    }

    onIssueClick(issue: Issue) {
        this.issueDialog.show(issue);
    }

    onIssueDragStart($event, issue) {
        $event.dataTransfer.setData('issueKey', issue.key);
    }

    reloadIssues() {
        this.projectService.getBacklog(this.projectKey).subscribe(issues => this.issues = issues);
    }

    onIssueDrop($event) {
        this.dragging = false;
        $event.preventDefault();
        let payload = JSON.parse($event.dataTransfer.getData('payload'));
        this.releaseService.moveBackIssue(payload.issueKey, payload.releaseKey).subscribe((issue: Issue) => {
            this.reloadIssues();
            this.backlogChanged.emit();
        });
    }

    onIssueDragOver($event) {
        $event.preventDefault();
        this.dragging = true;
    }

    onIssueDragEnd() {
        this.dragging = false;
    }
}
