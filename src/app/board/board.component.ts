import {Component, OnInit} from '@angular/core';
import {Board} from "../models/board";
import {ReleaseService} from "../services/release.service";
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
    releaseKey: string;
    board: Board;

    constructor(private route: ActivatedRoute, private service: ReleaseService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.releaseKey = params['release_key'];
            this.loadBoard();
        });
    }

    loadBoard() {
        this.service.getBoard(this.releaseKey).subscribe(board => this.board = board);
    }
}
