import {Component, OnInit, ElementRef, ViewChild, Output, EventEmitter} from '@angular/core';
import {Issue} from "../models/issue";
import {Task} from "../models/task";
import {IssueService} from "../services/issue.service";
import {ActivatedRoute} from '@angular/router';
import {TaskComponent} from "../task/task.component";
declare var $: any;

@Component({
    selector: 'app-issue',
    templateUrl: './issue.component.html',
    styleUrls: ['./issue.component.scss']
})
export class IssueComponent implements OnInit {
    issue: Issue;
    creatingTask: boolean = false;
    tasks: Task[];
    hasChanged: boolean = false;
    @ViewChild('issueDialog') me: ElementRef;
    @ViewChild('taskDialog') taskDialog: TaskComponent;
    @Output('issuechanged') issueChanged = new EventEmitter();

    constructor(private service: IssueService, private route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    show(issue: Issue) {
        this.tasks = [];
        this.issue = issue;
        this.hasChanged = false;
        $(this.me.nativeElement).modal();
        this.loadTasks();
    }

    private loadTasks() {
        this.service.getTasks(this.issue.key).subscribe(tasks => this.tasks = tasks);
    }

    onCreateTask() {
        this.creatingTask = true;
    }

    onTaskCreated($event) {
        this.creatingTask = false;
        this.service.saveTask(this.issue.key, $event.title).subscribe(task => this.loadTasks());
    }

    onTaskClick(task) {
        this.taskDialog.show(task);
    }

    onTitleChanged(newTitle) {
        this.issue.title = newTitle;
        this.hasChanged = true;
    }

    onKindChanged(newKind) {
        this.issue.kind = newKind;
        this.hasChanged = true;
    }

    onDescChanged(newDesc) {
        this.issue.desc = newDesc;
        this.hasChanged = true;
    }

    onSaveIssueChanges() {
        this.service.updateIssue(this.issue).subscribe(issue => {
            console.log(issue);
            this.issueChanged.emit();
        });
        $(this.me.nativeElement).modal('hide');
    }

    onTaskUpdated(task: Task) {
        this.service.updateTask(this.issue.key, task).subscribe((task: Task) => console.log(task));
        this.issueChanged.emit();
    }
}
