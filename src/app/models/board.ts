export interface Board {
    release: ReleaseBoard;
}

interface ReleaseBoard {
    version: string;
    key: string;
    status: number;
    issues: BoardIssue[];
}

interface BoardIssue {
    title: string;
    id: string;
    key: string;
    kind: string;
    tasks: BoardTaskCollection;
}

interface BoardTaskCollection {
    DONE: BoardTask[];
    IN_PROGRESS: BoardTask[];
    TO_DO: BoardTask[];
}

interface BoardTask {
    title: string;
    id: string;
    key: string;
}
