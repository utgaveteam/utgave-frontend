export interface Issue {
    id: number,
    title: string;
    desc: string;
    status: number;
    key: string;
    creation_date: Date;
    kind: string;
}
