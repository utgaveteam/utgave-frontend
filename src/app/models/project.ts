export interface Project {
    id: string;
    name: string;
    desc: string;
    key: string;
    creation_date: Date;
}
