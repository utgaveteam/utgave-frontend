export interface Release {
    version: string;
    status: number;
    creation_date: Date;
    key: string;
    released_date: Date;
}
