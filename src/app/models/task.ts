export interface Task {
    id: string;
    key: string;
    title: string;
    desc: string;
    status: number;
    creation_date: Date;
}
