import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'releaseStatus'
})
export class ReleaseStatusPipe implements PipeTransform {

    transform(value: number, args?: any): any {
        switch (value) {
            case 100:
                return 'NEW';
            case 150:
                return 'TO DO';
            case 200:
                return 'IN PROGRESS';
            case 250:
                return 'COMPLETED';
            case 300:
                return 'RELEASED';
            default:
                return 'UNKNOWN';
        }
    }

}
