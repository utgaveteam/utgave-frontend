/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import {By} from "@angular/platform-browser";

import {ProjectDashboardComponent} from './project-dashboard.component';
import {ActivatedRouteStub} from "../../testing/router-stubs";
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Project} from "../models/project";
import {ProjectService} from "../services/projects.service";

class ProjectServiceStub {
    getProject(key: string): Observable<Project> {
        return Observable.of(null);
    }
}

describe('ProjectDashboardComponent', () => {
    let component: ProjectDashboardComponent;
    let fixture: ComponentFixture<ProjectDashboardComponent>;
    //noinspection SpellCheckingInspection
    const projectKey = 'dsakjdasd98u098das';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProjectDashboardComponent],
            providers: [
                {provide: ActivatedRoute, useClass: ActivatedRouteStub},
                {provide: ProjectService, useClass: ProjectServiceStub},
            ]
        })
            .compileComponents();
    }));

    beforeEach(inject([ActivatedRoute], (route: ActivatedRouteStub) => {
        route.testParams = {project_key: projectKey};
        fixture = TestBed.createComponent(ProjectDashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('should get project info on init', inject([ProjectService], (service) => {
        const project: Project = {id: 'TPO', name: 'Test Project One', creation_date: new Date, key: projectKey};
        let spy = spyOn(service, 'getProject').and.returnValue(Observable.of(project));

        component.ngOnInit();

        expect(spy.calls.count()).toBe(1);
        expect(spy.calls.first().args[0]).toBe(projectKey);
        expect(component.project).toBe(project);
    }));
});
