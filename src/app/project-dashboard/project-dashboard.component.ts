import {Component, OnInit} from '@angular/core';

import {Project} from "../models/project";
import {ProjectService} from "../services/projects.service";
import {ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'utgave-project-dashboard',
    templateUrl: './project-dashboard.component.html',
    styleUrls: ['./project-dashboard.component.scss']
})
export class ProjectDashboardComponent implements OnInit {
    project: Project = null;

    constructor(private projectService: ProjectService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.switchMap((params: Params) => this.projectService.getProject(params['project_key']))
            .subscribe(project => this.project = project);
    }

}
