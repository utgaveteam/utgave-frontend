/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {ProjectFormComponent} from './project-form.component';
import {ReactiveFormsModule} from '@angular/forms'
import {ProjectService} from "../services/projects.service";
import {Router} from '@angular/router'
import {RouterStub} from "../../testing/router-stubs";
import {Observable} from "rxjs/Rx";
import {Project} from "../models/project";

class ProjectServiceStub {
    public saveProject(name: string): Observable<Project> {
        return Observable.of(null);
    }
}

describe('ProjectFormComponent', () => {
    let component: ProjectFormComponent;
    let fixture: ComponentFixture<ProjectFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProjectFormComponent],
            imports: [
                ReactiveFormsModule
            ],
            providers: [
                {provide: ProjectService, useClass: ProjectServiceStub},
                {provide: Router, useClass: RouterStub}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProjectFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('project is invalid on startup', () => {
        expect(component.project.invalid).toBeTruthy();
    });

    it('project is valid with required fields', () => {
        component.project.get('name').patchValue('Test Project');
        component.project.get('id').patchValue('TPO');
        expect(component.project.valid).toBeTruthy();
    });

    it('save project and go to project dashboard page', async(inject([ProjectService, Router], (service: ProjectService, router: Router) => {
        //noinspection SpellCheckingInspection
        const projectKey = 'dsadasdasffg333';
        let serviceSpy = spyOn(service, 'saveProject').and.returnValue(Observable.of({
            name: 'Test Feature',
            key: projectKey,
            creation_date: new Date,
            id: 'TPO'
        }));
        let routerSpy = spyOn(router, 'navigateByUrl');

        component.project.get('name').patchValue('Test Project');
        component.project.get('id').patchValue('TPO');
        expect(component.project.valid).toBeTruthy();

        component.onSubmit();
        fixture.detectChanges();

        expect(serviceSpy.calls.count()).toBe(1);
        expect(serviceSpy.calls.first().args[0]).toBe('Test Project');
        expect(serviceSpy.calls.first().args[1]).toBe('TPO');
        expect(routerSpy.calls.count()).toBe(1);
        expect(routerSpy.calls.first().args[0]).toBe(`/projects/${projectKey}/dashboard`);

    })));
});
