import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ProjectService} from '../services/projects.service';
import {Project} from '../models/project';
import {Router} from '@angular/router';

@Component({
    selector: 'app-project-form',
    templateUrl: './project-form.component.html',
    styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {
    project: FormGroup;

    constructor(private formBuilder: FormBuilder, private projectService: ProjectService, private router: Router) {
    }

    ngOnInit() {
        this.project = this.formBuilder.group({
            name: ['', Validators.required],
            id: ['', [Validators.required, Validators.maxLength(3), Validators.minLength(3)]],
            desc: ['']
        });
    }

    onSubmit() {
        this.projectService.saveProject(this.project.get('name').value,
            this.project.get('id').value,
            this.project.get('desc').value)
            .subscribe((project: Project) => this.router.navigateByUrl(`/projects/${project.key}/dashboard`));
    }

}
