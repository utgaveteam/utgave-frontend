/* tslint:disable:no-unused-variable */
import {async, inject, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {Observable} from 'rxjs/Rx';

import {ProjectSelectorComponent} from './project-selector.component';
import {ProjectService} from '../services/projects.service';
import {Project} from '../models/project';
import {Location} from '@angular/common';
import {LocationStub} from "../../testing/location-stub";

//noinspection SpellCheckingInspection
const project1Key = 'sadkjasd8796875674';
//noinspection SpellCheckingInspection
const project2Key = 'sadkjasd8796875dasd4';

class ProjectServiceStub {
    public projects: Project[] = [
        {
            id: 'TPO',
            name: 'Project 1',
            key: project1Key,
            creation_date: new Date,
        },
        {
            id: 'TP2',
            name: 'Project 2',
            key: project2Key,
            creation_date: new Date,
        }
    ];

    //noinspection JSUnusedGlobalSymbols
    public getProjects(): Observable<Project[]> {
        return Observable.of(this.projects);
    }
}

describe('ProjectSelectorComponent', () => {
    let component: ProjectSelectorComponent;
    let fixture: ComponentFixture<ProjectSelectorComponent>;
    let projectsService: ProjectService;
    let debugElement: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProjectSelectorComponent],
            providers: [
                {provide: ProjectService, useClass: ProjectServiceStub},
                {provide: Location, useClass: LocationStub}
            ]
        })
            .compileComponents();
    }));

    beforeEach(inject([Location], (location: LocationStub) => {
        location.setPath(`/projects/${project1Key}/dashboard`);
        fixture = TestBed.createComponent(ProjectSelectorComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        fixture.detectChanges();
        projectsService = fixture.debugElement.injector.get(ProjectService);
    }));

    it('should load projects on init', () => {
        expect(component.projects.length).toBe(2);
    });

    it('should select project from url when present', () => {
        let projectClicked: Project = null;
        component.projectSelected.subscribe((project) => projectClicked = project);
        component.ngOnInit();

        expect(projectClicked).toBeNull();
        expect(component.currentProject).toBe(component.projects.find(p => p.key == project1Key));
    });

    it('should select first project on init if not project on url', inject([Location], (location: LocationStub) => {
        location.setPath('/');
        let projectClicked: Project = null;
        component.projectSelected.subscribe((project) => projectClicked = project);
        component.ngOnInit();

        expect(projectClicked).toBe(component.projects.find(p => p.key == project1Key));
        expect(component.currentProject).toBe(component.projects.find(p => p.key == project1Key));
    }));

    it('should select a project if clicked', inject([Location], (location: LocationStub) => {
        location.setPath('/');
        let projectClicked: Project = null;
        component.projectSelected.subscribe((project) => projectClicked = project);
        component.onProjectClick(component.projects.find(p => p.key == project2Key));

        expect(projectClicked).toBe(component.projects.find(p => p.key == project2Key));
        expect(component.currentProject).toBe(component.projects.find(p => p.key == project2Key));
    }));

    it('should select a null project if new clicked', inject([Location], (location: LocationStub) => {
        location.setPath('/');
        let projectClicked: Project = null;
        component.projectSelected.subscribe((project) => projectClicked = project);
        component.onNewProjectClick();

        expect(projectClicked).toBeNull();
        expect(component.currentProject).toBeNull();
    }));
});
