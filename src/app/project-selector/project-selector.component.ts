import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {ProjectService} from '../services/projects.service';
import {Project} from '../models/project';
import {Location} from '@angular/common';

@Component({
    selector: 'utgave-project-selector',
    templateUrl: './project-selector.component.html',
    styleUrls: ['./project-selector.component.scss']
})
export class ProjectSelectorComponent implements OnInit {
    projects: Project[] = [];
    currentProject: Project = null;
    @Output() projectSelected = new EventEmitter<Project>();

    get allProjectsButSelected() {
        if (this.currentProject)
            return this.projects.filter(p => p.key != this.currentProject.key);
        return this.projects;
    }

    constructor(private projectService: ProjectService, private location: Location) {
    }

    ngOnInit() {
        this.projectService.getProjects().subscribe(projects => {
            const matches = this.location.path(false).match(/\/projects\/([^/]+)/);
            const projectKey = matches == null ? null : matches[1];

            this.projects = projects;
            this.currentProject = this.projects.find(p => p.key == projectKey);

            if (null == projectKey && null == this.currentProject && this.projects.length > 0) {
                this.currentProject = this.projects[0];
                this.onProjectClick(this.currentProject);
            }

        });
    }

    onProjectClick(project: Project) {
        this.currentProject = project;
        this.projectSelected.emit(this.currentProject);
    }

    onNewProjectClick() {
        this.onProjectClick(null);
    }

    // TODO: We should get back this
    // onNewProjectCreated(key: string) {
    //     this.projectService.getProjects().subscribe(projects => {
    //         this.projects = projects;
    //         let createdProject = this.projects.find(p => p.key == key);
    //         this.onProjectClick(createdProject);
    //     });
    // }
}
