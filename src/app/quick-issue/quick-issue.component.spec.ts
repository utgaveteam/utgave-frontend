/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {QuickIssueComponent} from './quick-issue.component';
import {ReactiveFormsModule} from '@angular/forms';

describe('QuickIssueComponent', () => {
    let component: QuickIssueComponent;
    let fixture: ComponentFixture<QuickIssueComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule],
            declarations: [QuickIssueComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuickIssueComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be false if required fields are empty', () => {
        component.issue.get('title').patchValue('');
        expect(component.issue.valid).toBeFalsy();
    });

    it('should emit event on submit', () => {
        component.issue.get('title').patchValue('Test Bug');
        let event = null;
        component.issueCreated.subscribe(e => event = e);
        component.onSubmit();

        expect(component.issue.valid).toBeTruthy();
        expect(event).not.toBeNull();
        expect(event.title).toBe('Test Bug');
        expect(event.kind).toBe('F');
    });
});
