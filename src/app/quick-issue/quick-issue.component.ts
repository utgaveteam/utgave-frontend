import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-quick-issue',
    templateUrl: './quick-issue.component.html',
    styleUrls: ['./quick-issue.component.scss']
})
export class QuickIssueComponent implements OnInit {
    issue: FormGroup;
    @Output() issueCreated = new EventEmitter();

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.issue = this.formBuilder.group({
            title: ['', Validators.required],
            kind: ['F', Validators.required]
        });
    }

    onSubmit() {
        this.issueCreated.emit({
            title: this.issue.get('title').value,
            kind: this.issue.get('kind').value,
        });
    }

}
