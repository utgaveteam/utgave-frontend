/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {QuickReleaseComponent} from './quick-release.component';

describe('QuickReleaseComponent', () => {
    let component: QuickReleaseComponent;
    let fixture: ComponentFixture<QuickReleaseComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [QuickReleaseComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuickReleaseComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
