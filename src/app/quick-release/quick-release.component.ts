import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-quick-release',
    templateUrl: './quick-release.component.html',
    styleUrls: ['./quick-release.component.scss']
})
export class QuickReleaseComponent implements OnInit {
    release: FormGroup;
    @Output() releaseCreated = new EventEmitter();

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.release = this.formBuilder.group({
            version: ['', Validators.required]
        });
    }

    onSubmit() {
        this.releaseCreated.emit({
            version: this.release.get('version').value,
        });
    }
}
