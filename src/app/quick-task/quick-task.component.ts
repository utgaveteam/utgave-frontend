import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-quick-task',
    templateUrl: './quick-task.component.html',
    styleUrls: ['./quick-task.component.scss']
})
export class QuickTaskComponent implements OnInit {
    task: FormGroup;
    @Output() taskCreated = new EventEmitter();

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.task = this.formBuilder.group({
            title: ['', Validators.required]
        });
    }

    onSubmit() {
        this.taskCreated.emit({
            title: this.task.get('title').value,
        });
    }
}
