/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {ReleaseListComponent} from './release-list.component';

describe('ReleaseListComponent', () => {
    let component: ReleaseListComponent;
    let fixture: ComponentFixture<ReleaseListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ReleaseListComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ReleaseListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
