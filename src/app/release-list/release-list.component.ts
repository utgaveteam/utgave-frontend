import {Component, OnInit} from '@angular/core';
import {Release} from "../models/release";
import {ViewChild} from "../../../node_modules/@angular/core/src/metadata/di";
import {ActivatedRoute} from "../../../node_modules/@angular/router/src/router_state";
import {ProjectService} from "../services/projects.service";
import {Params} from "../../../node_modules/@angular/router/src/shared";

@Component({
    selector: 'app-release-list',
    templateUrl: './release-list.component.html',
    styleUrls: ['./release-list.component.scss']
})
export class ReleaseListComponent implements OnInit {
    releases: Release[];
    creatingRelease: boolean = false;
    projectKey: string;
    // @ViewChild('issueDialog') issueDialog: IssueComponent;

    constructor(private route: ActivatedRoute, private projectService: ProjectService) {
    }

    ngOnInit() {
        this.route.params.switchMap((params: Params) => {
            this.projectKey = params['project_key'];
            return this.projectService.getReleases(this.projectKey);
        }).subscribe(releases => this.releases = releases);
    }

    onCreateRelease() {
        this.creatingRelease = true;
    }

    onReleaseCreated($event) {
        this.creatingRelease = false;
        this.projectService.saveRelease(this.projectKey, $event.version).subscribe(release => {
            this.projectService.getReleases(this.projectKey).subscribe(releases => this.releases = releases);
        });
    }

    onReleaseClick(release: Release) {

    }
}
