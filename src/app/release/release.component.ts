import {Component, OnInit, ViewChild} from '@angular/core';
import {Release} from "../models/release";
import {ActivatedRoute} from "../../../node_modules/@angular/router/src/router_state";
import {ReleaseService} from "../services/release.service";
import {Issue} from "../models/issue";
import {BacklogComponent} from "../backlog/backlog.component";
import {IssueComponent} from "../issue/issue.component";
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'app-release',
    templateUrl: './release.component.html',
    styleUrls: ['./release.component.scss']
})
export class ReleaseComponent implements OnInit {
    releaseKey: string;
    projectKey: string;
    release: Release = null;
    issues: Issue[] = [];
    dragging: boolean = false;
    showBacklog: boolean = false;
    @ViewChild('backlog') backlog: BacklogComponent;
    @ViewChild('issueDialog') issueDialog: IssueComponent;

    constructor(private route: ActivatedRoute, private service: ReleaseService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.releaseKey = params['release_key'];
            this.projectKey = params['project_key'];
            this.loadRelease();
            this.reloadIssues();
        });
    }

    loadRelease() {
        this.service.getRelease(this.releaseKey).subscribe(release => this.release = release);
    }

    reloadIssues() {
        this.service.getIssues(this.releaseKey).subscribe(issues => this.issues = issues);
    }

    onIssueDrop($event) {
        this.dragging = false;
        $event.preventDefault();
        let issueKey = $event.dataTransfer.getData('issueKey');
        this.service.moveIssue(issueKey, this.release.key).subscribe((issue: Issue) => {
            this.backlog.reloadIssues();
            this.reloadIssues();
        });
    }

    onIssueDragOver($event) {
        $event.preventDefault();
        this.dragging = true;
    }

    onIssueClick(issue: Issue) {
        this.issueDialog.show(issue);
    }

    onToggleBacklogClick() {
        this.showBacklog = !this.showBacklog;
    }

    onReleaseRelease() {
        let previousStatus = this.release.status;
        this.release.status = 300;
        this.service.updateRelease(this.release).catch((e, c) => {
            this.release.status = previousStatus;
            return Observable.of(this.release)
        }).subscribe((release: Release) => this.release = release);
    }

    onIssueChanged() {
        this.loadRelease();
    }

    onIssueDragStart($event, issue) {
        const payload = {
            'releaseKey': this.release.key,
            'issueKey': issue.key
        };
        $event.dataTransfer.setData('payload', JSON.stringify(payload));
    }

    onBacklogChanged() {
        this.reloadIssues();
    }

    onIssueDragEnd() {
        this.dragging = false;
    }

}
