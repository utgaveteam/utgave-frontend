import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Issue} from "../models/issue";
import {Task} from "../models/task";
import {Http, Response} from '@angular/http';
import {environment} from "../../environments/environment";

@Injectable()
export class IssueService {
    private apiUrl = `${environment.baseUrl}/issues`;

    private static extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private static handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.message || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        // return Observable.throw(errMsg);
        return null;
    }

    constructor(private http: Http) {
    }

    public getIssue(issueKey: string): Observable<Issue> {
        return this.http.get(`${this.apiUrl}/${issueKey}`).catch(IssueService.handleError).map(IssueService.extractData);
    }

    public getTasks(issueKey: string): Observable<Task[]> {
        return this.http.get(`${this.apiUrl}/${issueKey}/tasks`).catch(IssueService.handleError).map(IssueService.extractData);
    }

    saveTask(issueKey: string, title: string) {
        const body = {title: title};
        return this.http.post(`${this.apiUrl}/${issueKey}/tasks`, body).catch(IssueService.handleError).map(IssueService.extractData);
    }

    updateIssue(issue: Issue): Observable<Issue> {
        return this.http.put(`${this.apiUrl}/${issue.key}`, issue).catch(IssueService.handleError).map(IssueService.extractData);
    }

    updateTask(issueKey: string, task: Task) {
        return this.http.put(`${this.apiUrl}/${issueKey}/tasks/${task.key}`, task).catch(IssueService.handleError).map(IssueService.extractData);
    }
}
