/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {Http, BaseRequestOptions, Response, ResponseOptions} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';

import {ProjectService} from './projects.service';
import {Project} from "../models/project";
import {Issue} from "../models/issue";


describe('ProjectService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                ProjectService,
                {
                    provide: Http,
                    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
                        return new Http(backend, options);
                    },
                    deps: [MockBackend, BaseRequestOptions]
                }]
        });
    });

    it('should get projects from server', inject([ProjectService, MockBackend], (service: ProjectService, backend: MockBackend) => {
        //noinspection SpellCheckingInspection
        const firstProjectKey = 'ADSASDKJDSA24564321321';
        //noinspection SpellCheckingInspection
        const secondProjectKey = 'ADSASDKJDSA24564321321';
        const response = [
            {
                'id': 'TPO',
                'name': 'Test Project One',
                'key': firstProjectKey,
                'creation_date': new Date
            },
            {
                'id': 'TPT',
                'name': 'Test Project Two',
                'key': secondProjectKey,
                'creation_date': new Date
            }
        ];
        const baseResponse = new Response(new ResponseOptions({body: response}));
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(baseResponse));
        service.getProjects().subscribe((projects: Project[]) => {
            expect(projects.length).toBe(2);
            expect(projects[0].id).toBe('TPO');
            expect(projects[0].name).toBe('Test Project One');
            expect(projects[0].key).toBe(firstProjectKey);
            expect(projects[1].id).toBe('TPT');
            expect(projects[1].name).toBe('Test Project Two');
            expect(projects[1].key).toBe(secondProjectKey);
        });
    }));

    it('should get a project from server', inject([ProjectService, MockBackend], (service: ProjectService, backend: MockBackend) => {
        //noinspection SpellCheckingInspection
        const projectKey = 'ADSASDKJDSA24564321321';
        const response = {
            'id': 'TPO',
            'name': 'Test Project One',
            'key': projectKey,
            'creation_date': new Date
        };

        const baseResponse = new Response(new ResponseOptions({body: response}));
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(baseResponse));

        service.getProject(projectKey).subscribe((project: Project) => {
            expect(project).toBeTruthy();
            expect(project.id).toBe('TPO');
            expect(project.name).toBe('Test Project One');
            expect(project.key).toBe(projectKey);
        });
    }));

    it('should save a project on server', inject([ProjectService, MockBackend], (service: ProjectService, backend: MockBackend) => {
        //noinspection SpellCheckingInspection
        const projectKey = 'ADSASDKJDSA24564321321';
        const response = {
            'id': 'TPO',
            'name': 'Test Project One',
            'key': projectKey,
            'creation_date': new Date
        };

        const baseResponse = new Response(new ResponseOptions({body: response}));
        backend.connections.subscribe((c: MockConnection) => {
            let request_body = JSON.parse(c.request.getBody());
            expect(request_body.id).toBe('TPO');
            expect(request_body.name).toBe('Test Project One');
            return c.mockRespond(baseResponse);
        });
        service.saveProject('Test Project One', 'TPO').subscribe((project: Project) => {
            expect(project).toBeTruthy();
        });
    }));

    it('should save a quick issue on server', inject([ProjectService, MockBackend], (service: ProjectService, backend: MockBackend) => {
        //noinspection SpellCheckingInspection
        const projectKey = 'ADSASDKJDSA24564321321';
        const response = {
            'id': 'TPO.1',
            'title': 'Test Feature',
            'desc': 'This is a test feature',
            'status': 150,
            'key': 'ADSASDKJDSA2456432132132'
        };

        const baseResponse = new Response(new ResponseOptions({body: response}));
        backend.connections.subscribe((c: MockConnection) => {
            let request_body = JSON.parse(c.request.getBody());
            expect(request_body.title).toBe('Test Feature');
            expect(request_body.kind).toBe('F');
            return c.mockRespond(baseResponse);
        });
        service.saveIssue(projectKey, 'Test Feature', 'F').subscribe((issue: Issue) => {
            expect(issue).toBeTruthy();
        });
    }));

    it('should get project backlog', inject([ProjectService, MockBackend], (service: ProjectService, backend: MockBackend) => {
        //noinspection SpellCheckingInspection
        const response = [
            {
                'id': 'TPO.1',
                'title': 'Test Feature',
                'kind': 'F',
                'desc': 'This is a test feature',
                'status': 150,
                'key': 'asdsa34234',
                'creation_date': new Date
            },
            {
                'id': 'TPO.2',
                'title': 'Test Feature 2',
                'kind': 'F',
                'desc': 'This is a test feature 2',
                'status': 100,
                'key': 'dasd45435',
                'creation_date': new Date,
                'last_modified': new Date
            }
        ];

        const baseResponse = new Response(new ResponseOptions({body: response}));
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(baseResponse));
        //noinspection SpellCheckingInspection
        service.getBacklog('ADSASDKJDSA24564321321').subscribe((issues: Issue[]) => {
            expect(issues).toBeTruthy();
            expect(issues.length).toBe(2);
            const issue = issues[0]
            expect(issue.id).toBe('TPO.1');
            expect(issue.title).toBe('Test Feature');
            expect(issue.kind).toBe('F');
            expect(issue.desc).toBe('This is a test feature');
            expect(issue.status).toBe(150);
            //noinspection SpellCheckingInspection
            expect(issue.key).toBe('asdsa34234');
        });
    }));
});
