import {Injectable} from '@angular/core';
import {Project} from '../models/project';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Rx';
import {Issue} from "../models/issue";
import {Release} from "../models/release";

@Injectable()
export class ProjectService {
    private apiUrl = `${environment.baseUrl}/projects`;

    private static extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private static handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    constructor(private http: Http) {
    }

    public getProjects(): Observable<Project[]> {
        return this.http.get(this.apiUrl).map(ProjectService.extractData).catch(ProjectService.handleError);
    }

    public getProject(key: string): Observable<Project> {
        return this.http.get(`${this.apiUrl}/${key}`).map(ProjectService.extractData).catch(ProjectService.handleError);
    }

    public saveProject(name: string, id: string, desc: string): Observable<Project> {
        let body = {
            'name': name,
            'id': id,
            'desc': desc
        };
        return this.http.post(`${this.apiUrl}`, body).map(ProjectService.extractData).catch(ProjectService.handleError);
    }

    public getBacklog(projectKey: string): Observable<Issue[]> {
        return this.http.get(`${this.apiUrl}/${projectKey}/backlog`).map(ProjectService.extractData).catch(ProjectService.handleError);
    }

    public saveIssue(projectKey, title, kind): Observable<Issue> {
        let body = {
            'title': title,
            'kind': kind
        };
        return this.http.post(`${this.apiUrl}/${projectKey}/issues`, body).map(ProjectService.extractData).catch(ProjectService.handleError);
    }

    getReleases(projectKey: string): Observable<Release[]> {
        return this.http.get(`${this.apiUrl}/${projectKey}/releases`).map(ProjectService.extractData).catch(ProjectService.handleError);
    }

    saveRelease(projectKey: string, version: string): Observable<Release> {
        const body = {version: version};
        return this.http.post(`${this.apiUrl}/${projectKey}/releases`, body).map(ProjectService.extractData).catch(ProjectService.handleError);
    }
}
