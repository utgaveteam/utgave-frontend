/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {ReleaseService} from './release.service';
import {Http, BaseRequestOptions, Response, ResponseOptions} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {Release} from "../models/release";

describe('ReleaseService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                MockBackend,
                BaseRequestOptions,
                ReleaseService,
                {
                    provide: Http,
                    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
                        return new Http(backend, options);
                    },
                    deps: [MockBackend, BaseRequestOptions]
                }
            ]
        });
    });

    it('should save a release on server', inject([ReleaseService, MockBackend], (service: ReleaseService, backend: MockBackend) => {
        const response = {
            'version': '1.0.0',
            'status': 100,
            'features': [],
            'bugs': [],
            'key': 'dsakjhdkashjd',
            'creation_date': '',
            'last_modified': ''
        };

        //noinspection SpellCheckingInspection
        const projectKey = 'dslaksdkasdlkasd';

        const baseResponse = new Response(new ResponseOptions({body: response}));
        backend.connections.subscribe((c: MockConnection) => {
            expect(c.request.url).toContain(projectKey);
            let request_body = JSON.parse(c.request.getBody());
            expect(request_body.version).toBe('1.0.0');
            return c.mockRespond(baseResponse);
        });
        service.saveRelease(projectKey, '1.0.0').subscribe((release: Release) => {
            expect(release).toBeTruthy();
        });
    }));
});
