import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Release} from "../models/release";
import {environment} from "../../environments/environment";
import {Http, Response} from '@angular/http';
import {Board} from "../models/board";

@Injectable()
export class ReleaseService {
    private apiUrl = `${environment.baseUrl}/releases`;

    private static extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private static handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.message || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    constructor(private http: Http) {
    }

    getIssues(releaseKey: string) {
        return this.http.get(`${this.apiUrl}/${releaseKey}/issues`).map(ReleaseService.extractData)
            .catch(ReleaseService.handleError);
    }

    getRelease(releaseKey: string) {
        return this.http.get(`${this.apiUrl}/${releaseKey}`).map(ReleaseService.extractData)
            .catch(ReleaseService.handleError);
    }

    moveIssue(issueKey: string, releaseKey: string) {
        return this.http.put(`${this.apiUrl}/${releaseKey}/issues/${issueKey}`, {}).map(ReleaseService.extractData)
            .catch(ReleaseService.handleError);
    }

    updateRelease(release: Release): Observable<Release> {
        return this.http.put(`${this.apiUrl}/${release.key}`, release).map(ReleaseService.extractData)
            .catch(ReleaseService.handleError);
    }

    getBoard(releaseKey: string): Observable<Board> {
        return this.http.get(`${this.apiUrl}/${releaseKey}/board`).map(ReleaseService.extractData)
            .catch(ReleaseService.handleError);
    }

    moveBackIssue(issueKey: string, releaseKey: string) {
        return this.http.delete(`${this.apiUrl}/${releaseKey}/issues/${issueKey}`, {}).map(ReleaseService.extractData)
            .catch(ReleaseService.handleError);
    }
}
