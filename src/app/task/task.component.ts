import {Component, OnInit, ElementRef, ViewChild, Output, EventEmitter} from '@angular/core';
import {Task} from "../models/task";
declare var $: any;

@Component({
    selector: 'app-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
    task: Task;
    hasChanged: boolean = false;
    @ViewChild('taskDialog') me: ElementRef;
    @Output('taskupdated') taskUpdated = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    show(task: Task) {
        this.task = task;
        this.hasChanged = false;
        $(this.me.nativeElement).modal();
    }

    onCopyId() {
        let taskId: any = document.getElementById('taskId');
        taskId.select();
        document.execCommand('copy');
    }

    onTitleChanged(newTitle) {
        this.hasChanged = true;
        this.task.title = newTitle;
    }

    onStatusChanged(newStatus) {
        this.hasChanged = true;
        this.task.status = newStatus;
    }

    onDescChanged(newDesc) {
        this.hasChanged = true;
        this.task.desc = newDesc;
    }

    onSaveIssueChanges() {
        $(this.me.nativeElement).modal('hide');
        this.taskUpdated.emit(this.task);
    }

}
