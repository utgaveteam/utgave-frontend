/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {EditableDescComponent} from './editable-desc.component';

describe('EditableDescComponent', () => {
    let component: EditableDescComponent;
    let fixture: ComponentFixture<EditableDescComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EditableDescComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditableDescComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
