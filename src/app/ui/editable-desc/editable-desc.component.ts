import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-editable-desc',
    templateUrl: './editable-desc.component.html',
    styleUrls: ['./editable-desc.component.scss']
})
export class EditableDescComponent implements OnInit {
    @Input() desc: string = '';
    @Output() descChanged = new EventEmitter();
    editing: boolean = false;
    originalDesc: string;

    constructor() {
    }

    ngOnInit() {
        this.editing = false;
    }

    onEditDescClick() {
        this.editing = true;
        this.originalDesc = this.desc;
    }

    onSaveClick() {
        this.editing = false;
        if (this.originalDesc != this.desc)
            this.descChanged.emit(this.desc);
    }

    onCancelClick() {
        this.editing = false;
        this.desc = this.originalDesc;
    }

}
