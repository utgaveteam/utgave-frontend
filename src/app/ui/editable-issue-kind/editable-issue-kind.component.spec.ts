/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {EditableIssueKindComponent} from './editable-issue-kind.component';

describe('EditableIssueKindComponent', () => {
    let component: EditableIssueKindComponent;
    let fixture: ComponentFixture<EditableIssueKindComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EditableIssueKindComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditableIssueKindComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
