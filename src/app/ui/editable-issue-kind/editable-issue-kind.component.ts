import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-editable-issue-kind',
    templateUrl: './editable-issue-kind.component.html',
    styleUrls: ['./editable-issue-kind.component.scss']
})
export class EditableIssueKindComponent implements OnInit {
    @Input() kind: string = 'F';
    @Output() kindChanged = new EventEmitter();
    originalKind: string = '';

    constructor() {
    }

    ngOnInit() {
    }

    onIssueKindClick(kind) {
        if (kind == this.kind) return;

        this.kind = kind;
        this.kindChanged.emit(kind);
    }

    onKindClick() {
        this.originalKind = this.kind;
    }

}
