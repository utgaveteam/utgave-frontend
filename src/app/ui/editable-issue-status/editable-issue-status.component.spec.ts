/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {EditableIssueStatusComponent} from './editable-issue-status.component';

describe('EditableIssueStatusComponent', () => {
    let component: EditableIssueStatusComponent;
    let fixture: ComponentFixture<EditableIssueStatusComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EditableIssueStatusComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditableIssueStatusComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
