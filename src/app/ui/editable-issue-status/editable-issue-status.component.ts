import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-editable-issue-status',
    templateUrl: './editable-issue-status.component.html',
    styleUrls: ['./editable-issue-status.component.scss']
})
export class EditableIssueStatusComponent implements OnInit {
    statusCodes: number[] = [1000, 2000, 3000, 4000];
    @Input() status: number;
    @Input() kind: string;
    @Output() statusChanged = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    onStatusClick(status) {
        if (status == this.status) return;

        this.status = status;
        this.statusChanged.emit(status);
    }

}
