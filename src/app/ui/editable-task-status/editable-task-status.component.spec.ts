/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {EditableTaskStatusComponent} from './editable-task-status.component';

describe('EditableTaskStatusComponent', () => {
    let component: EditableTaskStatusComponent;
    let fixture: ComponentFixture<EditableTaskStatusComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EditableTaskStatusComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditableTaskStatusComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
