import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-editable-task-status',
    templateUrl: './editable-task-status.component.html',
    styleUrls: ['./editable-task-status.component.scss']
})
export class EditableTaskStatusComponent implements OnInit {
    taskStatus: number[] = [100, 200, 300, 400];
    @Input() status: number;
    @Output() statusChanged = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    onStatusClick(status) {
        if (status == this.status) return;

        this.status = status;
        this.statusChanged.emit(status);
    }
}
