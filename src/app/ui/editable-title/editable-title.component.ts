import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-editable-title',
    templateUrl: './editable-title.component.html',
    styleUrls: ['./editable-title.component.scss']
})
export class EditableTitleComponent implements OnInit {
    showEditButton: boolean = false;
    editing: boolean = false;
    originalValue: string;
    @Input() title: string = 'default value';
    @Output() titleChanged = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
        this.showEditButton = false;
        this.editing = false;
    }

    onMouseEnterOrLeave() {
        this.showEditButton = !this.showEditButton;
    }

    onEditClick() {
        this.editing = true;
        this.originalValue = this.title;
    }

    onSaveClick() {
        this.editing = false;
        this.showEditButton = false;

        if (this.originalValue != this.title)
            this.titleChanged.emit(this.title);
    }

    onCancelClick() {
        this.editing = false;
        this.showEditButton = false;
        this.title = this.originalValue;
    }
}
