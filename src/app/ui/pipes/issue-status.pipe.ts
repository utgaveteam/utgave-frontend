import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'issueStatus'
})
export class IssueStatusPipe implements PipeTransform {

    transform(value: number, args?: any): any {
        switch (value) {
            case 1000:
                return 'NEW';
            case 2000:
                return args[0] == 'F' ? 'TO DO' : 'TO FIX';
            case 3000:
                return 'IN PROGRESS';
            case 4000:
                return args[0] == 'F' ? 'DONE' : 'FIXED';
            default:
                return 'UNKNOWN';
        }
    }
}
