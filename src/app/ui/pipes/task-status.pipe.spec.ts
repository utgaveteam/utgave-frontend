/* tslint:disable:no-unused-variable */

import {TestBed, async} from '@angular/core/testing';
import {TaskStatusPipe} from './task-status.pipe';

describe('TaskStatusPipe', () => {
    it('should create an instance', () => {
        let pipe = new TaskStatusPipe();
        expect(pipe).toBeTruthy();
    });

    it('should transform a 100 to NEW', () => {
        let pipe = new TaskStatusPipe();
        expect(pipe.transform(100)).toBe('NEW');
    });

    it('should transform a 200 to TO DO', () => {
        let pipe = new TaskStatusPipe();
        expect(pipe.transform(200)).toBe('TO DO');
    });

    it('should transform a 300 to IN PROGRESS', () => {
        let pipe = new TaskStatusPipe();
        expect(pipe.transform(300)).toBe('IN PROGRESS');
    });

    it('should transform a 400 to DONE', () => {
        let pipe = new TaskStatusPipe();
        expect(pipe.transform(400)).toBe('DONE');
    });
});
