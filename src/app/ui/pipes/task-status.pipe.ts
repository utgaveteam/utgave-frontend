import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'taskStatus'
})
export class TaskStatusPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        switch (value) {
            case 100:
                return 'NEW';
            case 200:
                return 'TO DO';
            case 300:
                return 'IN PROGRESS';
            case 400:
                return 'DONE';
            default:
                return 'UNKNOWN';
        }
    }

}
