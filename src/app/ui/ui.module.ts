import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditableTitleComponent} from './editable-title/editable-title.component';
import {FormsModule} from '@angular/forms';
import {EditableIssueKindComponent} from './editable-issue-kind/editable-issue-kind.component';
import {EditableIssueStatusComponent} from './editable-issue-status/editable-issue-status.component';
import {IssueStatusPipe} from "./pipes/issue-status.pipe";
import {EditableDescComponent} from './editable-desc/editable-desc.component';
import {EditableTaskStatusComponent} from './editable-task-status/editable-task-status.component';
import {TaskStatusPipe} from "./pipes/task-status.pipe";

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        EditableTitleComponent,
        EditableIssueKindComponent,
        EditableIssueStatusComponent,
        IssueStatusPipe,
        EditableDescComponent,
        EditableTaskStatusComponent,
        TaskStatusPipe
    ],
    exports: [
        EditableTitleComponent,
        EditableIssueKindComponent,
        EditableIssueStatusComponent,
        IssueStatusPipe,
        EditableDescComponent,
        EditableTaskStatusComponent,
        TaskStatusPipe
    ]
})
export class UiModule {
}
