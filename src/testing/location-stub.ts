import {Injectable} from '@angular/core';

@Injectable()
export class LocationStub {
    private _path: string;

    public path(b: boolean): string {
        return this._path;
    }

    public setPath(path: string) {
        this._path = path;
    }
}
